from flask import Flask, render_template, request
import paho.mqtt.client as mqtt
import sqlite3
from datetime import datetime
import threading
import math
import matplotlib.pyplot as plt
from io import BytesIO
import time

app = Flask(__name__)

MQTT_BROKER = 'broker.hivemq.com'
MQTT_PORT = 1883
MQTT_CO2_TOPIC = 'co2'

def on_connect(client, userdata, flags, rc):
    print('Connected to MQTT broker')
    client.subscribe([(MQTT_CO2_TOPIC, 0)])

def on_message(client, userdata, msg):
    print('Received MQTT message')
    if msg.topic == MQTT_CO2_TOPIC:
        co2_str = msg.payload.decode()
        if is_valid_co2(co2_str):
            co2 = float(co2_str)
            update_data(co2)
        else:
            print('Invalid CO2 value:', co2_str)

def is_valid_co2(co2_str):
    try:
        co2 = float(co2_str)
        return not math.isnan(co2)
    except ValueError:
        return False

def update_data(co2):
    save_co2(co2)

def create_tables():
    conn = sqlite3.connect('co2_data.db')
    c = conn.cursor()

    c.execute('''CREATE TABLE IF NOT EXISTS co2 (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    value REAL NOT NULL,
                    timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP
                )''')

    conn.commit()
    conn.close()

def save_co2(co2):
    if co2 is not None:
        conn = sqlite3.connect('co2_data.db')
        c = conn.cursor()
        c.execute('INSERT INTO co2 (value, timestamp) VALUES (?, ?)', (co2, datetime.now()))
        conn.commit()
        conn.close()

def get_co2_data(start_time=None, end_time=None):
    conn = sqlite3.connect('co2_data.db')
    c = conn.cursor()

    query = 'SELECT timestamp, value FROM co2'
    if start_time and end_time:
        query += ' WHERE timestamp BETWEEN ? AND ?'
        c.execute(query, (start_time, end_time))
    else:
        c.execute(query)

    co2_data = c.fetchall()

    conn.close()

    return co2_data

def generate_co2_chart(start_time=None, end_time=None):
    co2_data = get_co2_data(start_time, end_time)
    timestamps = [row[0] for row in co2_data]
    values = [row[1] for row in co2_data]

    fig, ax = plt.subplots()

    if timestamps:
        ax.plot(timestamps, values)
        ax.set_xlim(min(timestamps), max(timestamps))
        ax.set_ylim(min(values), max(values))
        ax.set(xlabel='Timestamp', ylabel='CO2 Value', title='CO2 Data')
        ax.grid()

        # Uložíme graf do paměti
        buffer = BytesIO()
        plt.savefig(buffer, format='png')
        buffer.seek(0)

        return buffer
    else:
        return None

def generate_co2_chart_async(start_time=None, end_time=None):
    buffer = generate_co2_chart(start_time, end_time)
    # Uložení bufferu nebo další zpracování

def start_background_tasks():
    # Spustit samostatné vlákno pro generování grafů
    def worker():
        while True:
            # Pravidelně generovat grafy
            start_time = datetime.now()
            end_time = datetime.now()
            generate_co2_chart_async(start_time, end_time)
            time.sleep(60)  # Počkat 60 sekund

    thread = threading.Thread(target=worker, daemon=True)
    thread.start()

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        start_time = datetime.strptime(request.form['start_time'], '%Y-%m-%dT%H:%M')
        end_time = datetime.strptime(request.form['end_time'], '%Y-%m-%dT%H:%M')

        buffer = generate_co2_chart(start_time, end_time)

        if buffer is not None:
            return buffer.getvalue(), 200, {'Content-Type': 'image/png'}
        else:
            return 'No data available', 404

    co2_data = get_co2_data()
    return render_template('new_index.html', co2=co2_data)

if __name__ == '__main__':
    create_tables()

    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message

    client.connect(MQTT_BROKER, MQTT_PORT, 60)
    client.loop_start()

    # Spustit samostatné vlákno pro generování grafů
    start_background_tasks()

    app.run(debug=True, host='0.0.0.0')
