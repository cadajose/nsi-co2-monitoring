Tento kód vytváří jednoduchou webovou aplikaci pomocí frameworku Flask. Aplikace umožňuje získat data o CO2 měření z MQTT brokeru, ukládat je do databáze SQLite a generovat grafy na základě těchto dat.

Klíčové části kódu jsou:

- Definice cesty / jako hlavní stránky aplikace. Pokud se na této stránce provede POST požadavek (odeslání formuláře), získají se hodnoty startovního a koncového času z formuláře, a na základě těchto hodnot se generuje graf pomocí funkce generate_co2_chart. Výsledný graf je vrácen jako odpověď.

- Funkce generate_co2_chart získá data z databáze pro dané časové rozmezí a vygeneruje graf pomocí knihovny Matplotlib. Graf je uložen do paměti jako PNG soubor v bufferu a vrácen zpět.

- Spuštění samostatného vlákna start_background_tasks, které periodicky generuje grafy. Toto vlákno běží nezávisle na hlavním vlákně aplikace a pravidelně generuje grafy v intervalu 60 sekund.

- Připojení k MQTT brokeru pomocí klienta mqtt.Client a nastavení obslužných funkcí on_connect a on_message. Klient se připojuje k brokeru a odebírá zprávy z tématu CO2.

- Vytvoření databáze SQLite a její inicializace pomocí funkce create_tables. Tabulka co2 obsahuje sloupce pro ID, hodnotu CO2 a časovou značku.


Data jsou získávána ze senzoru SGP30 přes raspberry pi pico W.

Importuje potřebné moduly: adafruit_sgp30 pro ovládání senzoru SGP30, machine pro ovládání hardwarových periferií, time pro časové prodlevy, network pro připojení k WiFi síti a umqtt.simple pro jednoduchou komunikaci přes MQTT.

Nastavuje připojení k WiFi síti pomocí modulu network.WLAN. Čeká na úspěšné připojení k síti.

Inicializuje I2C komunikaci pomocí modulu machine.I2C. Používá piny GP1 (SCL) a GP0 (SDA) pro komunikaci s senzorem.

Vytváří instanci třídy adafruit_sgp30.Adafruit_SGP30 pro komunikaci se senzorem SGP30 přes I2C.

Nastavuje připojení k MQTT brokeru definovaného v proměnných SERVER a PORT. Vytváří instanci třídy mqtt.MQTTClient a připojuje se k brokeru.

V nekonečné smyčce provádí měření hodnot CO2eq a TVOC pomocí funkce sgp30.iaq_measure(). Výsledky jsou vypsány na konzoli.

Vytváří zprávu pro odeslání přes MQTT, ve které se používá pouze hodnota CO2eq.

Odesílá zprávu přes MQTT pomocí mqtt_client.publish(). Zpráva je publikována na téma (topic) "co2".

Čeká 1 sekundu pomocí time.sleep(1) a opakuje smyčku pro další měření a odeslání.

Tento kód slouží k pravidelnému měření hodnot CO2eq a TVOC pomocí senzoru SGP30 a odesílání těchto hodnot přes MQTT protokol.
