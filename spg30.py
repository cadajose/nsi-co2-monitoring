import adafruit_sgp30
from machine import Pin, I2C
import time
import network
import umqtt.simple as mqtt

wlan = network.WLAN(network.STA_IF)
wlan.active(True)
wlan.connect('Josef', 'burtcajk')
while not wlan.isconnected():
    pass
print('Connected to Wi-Fi')

i2c = I2C(1, scl=Pin(19), sda=Pin(18))  # Piny GP1 (SCL) a GP0 (SDA) pro I2C komunikaci

sgp30 = adafruit_sgp30.Adafruit_SGP30(i2c)
sgp30.iaq_init()

SERVER = 'broker.hivemq.com'
PORT = 1883
MQTT_TOPIC = 'co2'

mqtt_client = mqtt.MQTTClient("pico_client", SERVER, PORT)
mqtt_client.connect()

while True:
    co2eq, tvoc = sgp30.iaq_measure()
    print("CO2eq = %d ppm \t TVOC = %d ppb" % (co2eq, tvoc))

    # Příprava zprávy pro MQTT
    message = str(co2eq)

    # Odeslání zprávy přes MQTT
    mqtt_client.publish(MQTT_TOPIC, message)

    time.sleep(1)


